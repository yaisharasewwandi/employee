@extends('layouts.backend.master')
@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Employee Edit Form 
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Employee</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('employee.update',$employee->id) }}" method="post" enctype="multipart/form-data">
              @csrf
               @method('PUT')
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Employee First Name</label>
                  <input type="text" class="form-control" value="{{$employee->fname}}" name='fname' id="exampleInputEmail1" placeholder="Enter name" required="required">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Employee Last Name</label>
                  <input type="text" class="form-control" value="{{$employee->lname}}" name='lname' id="exampleInputEmail1" placeholder="Enter name" required="required">
                </div>
                 <div class="form-group">
                  <label>Select Company</label>
                  <select class="form-control" name="company_id">
                    <option>Select Company</option>
                    @foreach ($company as $key => $value)
                    <option value="{{ $key }}" {{ ( $key == $selectedID) ? 'selected' : '' }}> 

                      {{$value}}
                      </option>
                     @endforeach   
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Employee Email</label>
                  <input type="text" class="form-control" value="{{$employee->personal_email}}" name='personal_email' id="exampleInputEmail1" placeholder="Enter email" required="required">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Employee Phone number</label>
                  <input type="text" class="form-control" value="{{$employee->phone_number}}" name='phone_number' id="exampleInputEmail1" placeholder="Enter number" required="required">
                </div>
              </div>
             
              
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
      </div>
  </div>
</section>
</div>

@endsection