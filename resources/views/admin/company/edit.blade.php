@extends('layouts.backend.master')
@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Company Edit Form 
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Company</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('company.update',$company->id) }}" method="post" enctype="multipart/form-data">
              @csrf
               @method('PUT')
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Company Name</label>
                  <input type="text" class="form-control" value="{{$company->name}}" name='name' id="exampleInputEmail1" placeholder="Enter name" required="required">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Company Email</label>
                  <input type="text" class="form-control" value="{{$company->email}}" name='email' id="exampleInputEmail1" placeholder="Enter email" required="required">
                </div>
              </div>
              @if (!empty($company->image))
              <div class="form-group col-12">
                  <img  id="upload-image" width="100px" src="{{asset('uploads/company/'.$company->image)}}" alt="" />
              </div>
              @endif
              <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile" name="image">

                  <p class="help-block">Company Logo </p>
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
      </div>
  </div>
</section>
</div>

@endsection