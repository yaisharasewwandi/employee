-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2020 at 08:32 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_category`
--

CREATE TABLE `banner_category` (
  `web_banner_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner_category`
--

INSERT INTO `banner_category` (`web_banner_id`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 23, '2020-01-15 04:07:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 23, '2020-01-15 04:07:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 21, '2020-01-15 04:14:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image` varchar(205) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_display` int(10) DEFAULT '2',
  `status_id` int(11) NOT NULL,
  `is_display_banner` int(11) NOT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_by` int(12) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `lft`, `rgt`, `name`, `slug`, `image`, `banner_image`, `user_id`, `is_display`, `status_id`, `is_display_banner`, `depth`, `order_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 1, 834, 'Root', 'root', NULL, '', 1, 1, 1, 0, 0, 1, '2017-08-10 06:00:00', '2019-10-03 07:04:13', NULL),
(20, 1, 596, 769, 'Electronics', 'electronics', 'uploads/category/electronics.jpg', '', 3, 1, 1, 0, 1, 1, '2018-01-05 15:03:56', '2019-10-03 07:04:13', NULL),
(21, 1, 770, 823, 'Phones & Tabs', 'phones-tablets', 'uploads/category/phones--accessories.jpg', '', 1, 1, 1, 0, 1, 1, '2018-01-05 15:07:13', '2019-10-03 07:04:13', NULL),
(22, 1, 340, 445, 'Fashion', 'Fashion', 'uploads/category/fashion.jpg', '', 3, 1, 1, 0, 1, 1, '2018-01-05 15:18:09', '2019-10-03 07:04:13', NULL),
(23, 1, 46, 145, 'Home Needs', 'adminsemployee', 'uploads/category/home-needs.jpg', '', 3, 1, 1, 0, 1, 1, '2018-01-05 15:19:40', '2020-01-15 06:27:08', NULL),
(24, 1, 146, 225, 'Health & Beauty', 'health-beauty', 'uploads/category/health--beauty.jpg', '', 3, 1, 1, 0, 1, 1, '2018-01-05 15:20:37', '2019-10-03 07:04:13', NULL),
(25, 1, 246, 339, 'Baby & Kids', 'baby-kids', 'uploads/category/baby--kids.jpg', '', 3, 1, 1, 0, 1, 1, '2018-01-05 15:25:23', '2019-10-03 07:04:13', NULL),
(26, 1, 546, 595, 'Sports & Fitness', 'sport-fitness', 'uploads/category/sports--fitness.jpg', '', 3, 1, 1, 0, 1, 1, '2018-01-05 15:26:30', '2019-10-03 07:04:13', NULL),
(27, 1, 226, 245, 'Daily Essential', 'daily-essential', 'uploads/category/essentials.jpg', '', 3, 2, 1, 0, 1, 1, '2018-01-05 15:29:45', '2019-10-03 07:04:13', NULL),
(28, 20, 733, 748, 'TV\'s', 'tv', 'uploads/category/televisions.jpg', 'uploads/category-banner/kkk22.jpg', 18, 1, 1, 2, 2, 1, '2018-01-18 12:08:24', '2019-10-03 07:04:13', NULL),
(29, 20, 617, 634, 'Audio & Video', 'audioandvideo', 'uploads/category/audio--video.jpg', '', 3, 1, 1, 0, 2, 1, '2018-01-18 12:09:39', '2019-10-03 07:04:13', NULL),
(30, 29, 618, 619, 'DVD Player', 'dvd-player', 'uploads/category/audio-player.jpg', '', 1, 1, 1, 0, 3, 1, '2018-01-18 12:11:16', '2019-10-03 07:04:13', NULL),
(31, 29, 620, 621, 'Home Theaters', 'home-theaters', 'uploads/category/audio-player.jpg', '', 1, 1, 1, 0, 3, 1, '2018-01-18 12:12:18', '2019-10-03 07:04:13', NULL),
(32, 29, 630, 631, 'Headphones & Earphones', 'headphonesandearphones', 'uploads/category/head-phone.jpg', '', 3, 1, 1, 0, 3, 1, '2018-01-18 12:14:20', '2019-10-03 07:04:13', NULL),
(33, 28, 74, 75, 'LED TV', 'led-tv', 'uploads/category/tv.jpg', '', 1, 1, 1, 0, 3, 1, '2018-01-18 12:15:29', '2018-05-04 12:30:23', '2018-05-04 12:30:23'),
(34, 28, 74, 75, 'LED Smart Tv', 'led-smart-tv', 'uploads/category/tv.jpg', '', 1, 1, 1, 0, 3, 1, '2018-01-18 12:16:01', '2018-05-04 12:30:36', '2018-05-04 12:30:36'),
(35, 28, 74, 75, 'LED 3D Smart Tv', 'led-3d-smart-tv', 'uploads/category/tv.jpg', '', 1, 1, 1, 0, 3, 1, '2018-01-18 12:16:51', '2018-05-04 12:30:48', '2018-05-04 12:30:48'),
(36, 20, 749, 768, 'Refrigerators', 'refrigerator', 'uploads/category/refrigerators.jpg', 'uploads/category-banner/inverter-category-banner-1.jpg', 18, 1, 1, 1, 2, 1, '2018-01-18 12:17:53', '2019-10-03 07:04:13', NULL),
(37, 36, 622, 623, 'Built In', 'built-in-refrigerator', 'uploads/category/refrigerator.jpg', '', 3, 1, 1, 0, 3, 1, '2018-01-18 12:20:31', '2019-04-17 06:43:06', '2019-04-17 06:43:06'),
(38, 36, 756, 757, 'Single Door', 'single-door-refrigerators', 'uploads/category/refrigerator.jpg', '', 3, 1, 1, 0, 3, 1, '2018-01-18 12:21:31', '2019-10-03 07:04:13', NULL),
(39, 36, 766, 767, 'Double Door', 'doubledoorrefrigerators', 'uploads/category/refrigerator.jpg', '', 18, 1, 1, 0, 3, 1, '2018-01-18 12:22:31', '2019-10-03 07:04:13', NULL),
(41, 40, 43, 44, 'Washer Dryer', 'washer-dryer', 'uploads/category/washing-machine.jpg', '', 1, 1, 1, 0, 2, 1, '2018-01-18 12:25:42', '2018-05-22 17:04:39', NULL),
(42, 40, 45, 46, 'Semi Auto Washing Machine', 'semi-auto-washing-machine', 'uploads/category/washing-machine.jpg', '', 1, 1, 1, 0, 2, 1, '2018-01-18 12:28:11', '2018-06-01 10:29:51', '2018-06-01 10:29:51'),
(43, 102, 644, 645, 'Irons', 'iron', 'uploads/category/iron.jpg', '', 3, 1, 1, 0, 3, 1, '2018-01-19 10:17:56', '2019-10-03 07:04:13', NULL),
(44, 110, 666, 667, 'Microwave Ovens', 'microwave-ovens', 'uploads/category/oven.jpg', '', 3, 1, 1, 0, 3, 1, '2018-01-19 10:20:08', '2019-10-03 07:04:13', NULL),
(45, 110, 684, 685, 'Cookers & Gas Cookers', 'cookersgascookers', 'uploads/category/gas-cookers.jpg', '', 18, 1, 1, 0, 3, 1, '2018-01-19 10:21:03', '2019-10-03 07:04:13', NULL),
(46, 110, 676, 677, 'Blenders & Grinders', 'blendersngrinders', 'uploads/category/blender.jpg', '', 3, 1, 1, 0, 3, 1, '2018-01-19 10:21:48', '2019-10-03 07:04:13', NULL),
(47, 110, 668, 669, 'Rice Cookers', 'rice-cookers', 'uploads/category/rice-cookers.jpg', '', 3, 1, 1, 0, 3, 1, '2018-01-19 10:38:51', '2019-10-03 07:04:13', NULL),
(48, 20, 719, 732, 'Cameras', 'cameraa', 'uploads/category/camera27s--accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-03-29 15:53:20', '2019-10-03 07:04:13', NULL),
(49, 21, 813, 822, 'Phones', 'mobile_phones', 'uploads/category/mobile-phones.jpg', '', 18, 1, 1, 0, 2, 1, '2018-04-27 11:38:46', '2019-10-03 07:04:13', NULL),
(50, 20, 597, 604, 'Washing Machines', 'washing-machines', 'uploads/category/washing-machines.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-02 12:53:14', '2019-10-03 07:04:13', NULL),
(51, 102, 646, 647, 'Dishwashers', 'Dishwashers', 'uploads/category/91mtvsptolsl1500.jpg', '', 3, 2, 1, 0, 3, 1, '2018-05-02 13:15:24', '2019-10-03 07:04:13', NULL),
(52, 20, 635, 642, 'A/C\'s & Air Coolers', 'ac_aircoolers', 'uploads/category/acs.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-04 12:07:20', '2019-10-03 07:04:13', NULL),
(53, 52, 638, 639, 'Inverter AC\'s', 'inverter_ac', 'uploads/category/webpnet-resizeimage-4.jpg', '', 3, 2, 1, 0, 3, 1, '2018-05-04 12:09:32', '2019-10-03 07:04:13', NULL),
(54, 52, 636, 637, 'Non-Inverter AC\'s', 'non_inverter_ac', 'uploads/category/inverter-vs-non-inverter-air-conditioner-unit.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:14:32', '2019-10-03 07:04:13', NULL),
(55, 48, 720, 721, 'DSLR Camera', 'dslr_camera', 'uploads/category/download.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:15:52', '2019-10-03 07:04:13', NULL),
(56, 48, 722, 723, 'Digital Cameras', 'digital_cameras', 'uploads/category/download-1.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:16:33', '2019-10-03 07:04:13', NULL),
(57, 48, 730, 731, 'Security Cameras & DVR\'s', 'security_camerandvr', 'uploads/category/41igvrgu0xlsl500acss350.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:17:41', '2019-10-03 07:04:13', NULL),
(58, 52, 640, 641, 'Air Curtains & Air Coolers', 'air_coolers', 'uploads/category/technoking-room-cooler-breeze-500x500.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-04 12:18:41', '2019-10-03 07:04:13', NULL),
(59, 28, 740, 741, '24 Inch & Below', '24_and_below', 'uploads/category/bcc719fa-728f-4b7f-9c31-835d89082d48198cbb3b304bab3c72a3fd48ea3e49d4d.jpeg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:37:28', '2019-10-03 07:04:13', NULL),
(60, 28, 734, 735, '32 Inch & Above', '32_inch_above', 'uploads/category/417amohkolsl500acss350.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:39:07', '2019-10-03 07:04:13', NULL),
(61, 28, 736, 737, '40 Inch & Above', '40_and_above', 'uploads/category/download-2.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:42:10', '2019-10-03 07:04:13', NULL),
(62, 28, 738, 739, '50 Inch & Above', '50_and_above', 'uploads/category/download-3.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:43:03', '2019-10-03 07:04:13', NULL),
(63, 28, 742, 743, '65 Inch & Above', '65_and_above', 'uploads/category/13653318f520.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:46:39', '2019-10-03 07:04:13', NULL),
(64, 28, 744, 745, 'TV Accessories', 'tv_accessories', 'uploads/category/tv-accessories-mounts.jpg', '', 3, 1, 1, 2, 3, 1, '2018-05-04 12:48:31', '2019-10-03 07:04:13', NULL),
(65, 29, 622, 623, 'Soundbar', 'soundbar', 'uploads/category/images.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:55:30', '2019-10-03 07:04:13', NULL),
(66, 29, 624, 625, 'Microphones', 'microphones', 'uploads/category/620524000000000-00-500x500.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:57:42', '2019-10-03 07:04:13', NULL),
(67, 29, 626, 627, 'Speakers', 'speakers', 'uploads/category/sa-1552pair1000.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:58:50', '2019-10-03 07:04:13', NULL),
(68, 36, 750, 751, 'Freezers', 'freezers', 'uploads/category/freezers-chest.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 12:59:57', '2019-10-03 07:04:13', NULL),
(69, 36, 752, 753, 'Bottle Coolers', 'bootle_coolers', 'uploads/category/download-4.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 13:00:58', '2019-10-03 07:04:13', NULL),
(70, 36, 754, 755, 'Mini Bar', 'minibar', 'uploads/category/download-6.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 13:02:15', '2019-10-03 07:04:13', NULL),
(71, 59, 593, 594, 'LED', 'led', 'uploads/category/417amohkolsl500acss350.jpg', '', 3, 1, 1, 0, 4, 1, '2018-05-04 14:00:12', '2018-06-06 16:51:18', '2018-06-06 16:51:18'),
(72, 48, 724, 725, 'Action Cameras', 'action_cameras', 'uploads/category/actionx1casefrontangle.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 14:02:18', '2019-10-03 07:04:13', NULL),
(73, 48, 726, 727, 'Drones', 'drones', 'uploads/category/c4f577b7-43c0-4b95-b28e-e2a59d766a602d9afaa2d941a32f06474c6f198ad1c6d.jpeg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 14:03:19', '2019-10-03 07:04:13', NULL),
(74, 48, 728, 729, 'Accessories', 'camera_accessories', 'uploads/category/714yx0pxm5lsx425.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 14:04:46', '2019-10-03 07:04:13', NULL),
(75, 80, 690, 697, 'Laptops', 'laptops', 'uploads/category/windows-laptops.png', '', 3, 1, 1, 0, 3, 1, '2018-05-04 15:41:37', '2019-10-03 07:04:13', NULL),
(76, 75, 691, 692, 'i3 Laptops & Notebooks', 'i3_laptopsandnotbooks', 'uploads/category/windows-laptops.png', '', 3, 1, 2, 0, 4, 1, '2018-05-04 15:43:18', '2019-10-03 07:04:13', NULL),
(77, 75, 693, 694, 'i5 Laptops & Notebooks', 'i5laptopsandnotebooks', 'uploads/category/windows-laptops.png', '', 3, 1, 2, 0, 4, 1, '2018-05-04 15:45:13', '2019-10-03 07:04:13', NULL),
(78, 75, 695, 696, 'i7 Laptops & Notebooks', 'i7aptopsandnotebooks', 'uploads/category/windows-laptops.png', '', 3, 1, 2, 0, 4, 1, '2018-05-04 15:47:34', '2019-10-03 07:04:13', NULL),
(79, 80, 706, 707, 'MacBook', 'macbooks', 'uploads/category/download-8.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-04 15:48:25', '2019-10-03 07:04:13', NULL),
(80, 20, 689, 708, 'Laptops & Computers', 'laptopsandcomputers', 'uploads/category/laptops--computers-1.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-04 16:03:19', '2019-10-03 07:04:13', NULL),
(81, 80, 698, 699, 'Desktops', 'dekstops', 'uploads/category/download-9.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 16:05:54', '2019-10-03 07:04:13', NULL),
(82, 80, 700, 701, 'Printers', 'printers', 'uploads/category/468065-canon-pixma-tr8520-wireless-home-office-all-in-one-printer.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 16:06:58', '2019-10-03 07:04:13', NULL),
(83, 80, 702, 703, 'Accessories', 'accessories', 'uploads/category/23946am24092016339.png', '', 3, 1, 1, 0, 3, 1, '2018-05-04 16:08:27', '2019-10-03 07:04:13', NULL),
(84, 50, 598, 599, 'Semi Automatic', 'semiautomatic', 'uploads/category/71mrf0ctwblsy445.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 17:24:45', '2019-10-03 07:04:13', NULL),
(85, 50, 600, 601, 'Fully Automatic', 'fullyautomatic', 'uploads/category/8408266-electrolux-65kg-et65sarm-top-loading-fully-automatic-washing-machine-picture-large.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 17:25:39', '2019-10-03 07:04:13', NULL),
(86, 50, 566, 567, 'Top Loading', 'toploading', 'uploads/category/8408266-electrolux-65kg-et65sarm-top-loading-fully-automatic-washing-machine-picture-large.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 17:32:07', '2018-05-30 14:43:10', '2018-05-30 14:43:10'),
(87, 50, 568, 569, 'Front Loading', 'frontloading', 'uploads/category/frontloaderwashingmachines.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-04 17:32:39', '2018-05-30 14:42:49', '2018-05-30 14:42:49'),
(88, 49, 814, 815, 'iPhones', 'iphones', 'uploads/category/iphone-x-gray-select-2017.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 11:49:18', '2019-10-03 07:04:13', NULL),
(89, 49, 816, 817, 'Smart Phones', 'smartphones', 'uploads/category/59adf6dcd29f4.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 11:50:12', '2019-10-03 07:04:13', NULL),
(90, 49, 818, 819, 'Feature Phones', 'featurephones', 'uploads/category/images.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 11:51:51', '2019-10-03 07:04:13', NULL),
(91, 21, 801, 812, 'Tablets', 'tablets', 'uploads/category/tablets.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 11:54:09', '2019-10-03 07:04:13', NULL),
(92, 91, 802, 803, 'Small - 8 Inch & Below', '8inchandbelow', 'uploads/category/download.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 11:55:32', '2019-10-03 07:04:13', NULL),
(93, 91, 804, 805, 'Medium - 8 to 10 Inch', 'mediume8to10inche', 'uploads/category/download-1.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 11:58:40', '2019-10-03 07:04:13', NULL),
(94, 91, 806, 807, 'Large - Over 10 Inch', 'largeover10inch', 'uploads/category/lenovo-yoga-tablet-2-10-inch-main400-wide.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 12:30:11', '2019-10-03 07:04:13', NULL),
(95, 21, 771, 792, 'Phone Accessories', 'phoneaccessories', 'uploads/category/accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 12:37:36', '2019-10-03 07:04:13', NULL),
(96, 95, 772, 773, 'Power Banks', 'powerbanks', 'uploads/category/mi-power-bank-pro-10000mah-slim.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 12:39:30', '2019-10-03 07:04:13', NULL),
(97, 95, 774, 775, 'Memory Cards', 'memorycard', 'uploads/category/51j0d8cqlsy355.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 12:40:25', '2019-10-03 07:04:13', NULL),
(98, 95, 788, 789, 'Handsfree & Bluetooth Headsets', 'bluetoothheadsets', 'uploads/category/41wst4hzejlsy355.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-09 12:41:21', '2019-10-03 07:04:13', NULL),
(99, 95, 782, 783, 'Data Cables & Chargers', 'datacableschargers', 'uploads/category/xs-06-b.png', '', 3, 1, 1, 0, 3, 1, '2018-05-09 12:42:31', '2019-10-03 07:04:13', NULL),
(100, 95, 776, 777, 'Mount, Stand & Holders', 'mountstandholders', 'uploads/category/energypal-h48ck-mount.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 13:03:21', '2019-10-03 07:04:13', NULL),
(101, 95, 778, 779, 'Wireless Devices', 'wirelessdevices', 'uploads/category/yoobao-wireless-charger-fast-charging-pad-mobile-phone-qi-enabled-devices-charger-for-samsung-s8-s7jpg640x640.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 13:25:31', '2019-10-03 07:04:13', NULL),
(102, 20, 643, 664, 'Home Appliances', 'homeappliances', 'uploads/category/home-appliances.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 14:48:43', '2019-10-03 07:04:13', NULL),
(103, 23, 67, 80, 'Dining Accessories', 'diningaccessories', 'uploads/category/dining-accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 14:58:03', '2018-06-01 10:56:11', NULL),
(104, 23, 127, 144, 'Home & Kitchen Accessories', 'homekitchenaccessories', 'uploads/category/kitchen-accessories.jpg', '', 18, 1, 1, 0, 2, 1, '2018-05-09 15:25:11', '2019-01-22 04:31:14', NULL),
(105, 23, 47, 66, 'Bed & Bath Accessories', 'bed-bath-accessories', 'uploads/category/bed--bath-accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 15:50:11', '2018-07-31 09:20:02', NULL),
(106, 23, 81, 92, 'Paints & Accessories', 'Paintsandaccessories', 'uploads/category/paints--accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 15:51:27', '2018-08-07 10:57:25', NULL),
(107, 23, 109, 126, 'Furniture & Home Decorations', 'Furnitureandhomedecorations', 'uploads/category/furniture--home-decorations.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 15:54:56', '2019-01-22 04:31:14', NULL),
(108, 23, 101, 108, 'Lighting', 'Lighting', 'uploads/category/lighting-n-decore.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 15:57:08', '2019-01-22 04:31:14', NULL),
(109, 23, 93, 100, 'Tools & Hardwares', 'ToolsandHardwares', 'uploads/category/tools--hardwares.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 15:59:51', '2018-08-07 10:57:25', NULL),
(110, 20, 665, 688, 'Cooking Appliances', 'Cookingappliances', 'uploads/category/cooking-appliances.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-09 16:09:37', '2019-10-03 07:04:13', NULL),
(111, 29, 258, 259, 'Projectors', 'projectors', 'uploads/category/download-4.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-09 16:22:20', '2018-05-17 12:46:20', '2018-05-17 12:46:20'),
(112, 25, 247, 260, 'Baby Care & Gear', 'BabyCareandGear', 'uploads/category/baby-care--gear.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:13:07', '2019-10-03 07:04:13', NULL),
(113, 25, 287, 294, 'Feeding & Nursing', 'Feeding&Nursing', 'uploads/category/feeding--nursing.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:18:54', '2019-10-03 07:04:13', NULL),
(114, 25, 305, 316, 'Kids Clothing', 'KidsClothing', 'uploads/category/kids-clothing.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:19:38', '2019-10-03 07:04:13', NULL),
(115, 25, 277, 286, 'Bath & Skin Care', 'BathSkinCarebaby', 'uploads/category/bath--skin-care.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:20:30', '2019-10-03 07:04:13', NULL),
(116, 25, 317, 330, 'Toys, Games & Furniture', 'ToysGames&Furniture', 'uploads/category/toys--games.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:21:30', '2019-10-03 07:04:13', NULL),
(117, 25, 261, 276, 'Baby Clothing', 'BabyClothing', 'uploads/category/baby-clothing.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:22:14', '2019-10-03 07:04:13', NULL),
(118, 25, 331, 338, 'Maternity Needs', 'MaternityNeeds', 'uploads/category/maternity-needs.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:23:58', '2019-10-03 07:04:13', NULL),
(119, 24, 147, 156, 'Make Up', 'Make-Up', 'uploads/category/make-up.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:26:13', '2019-06-25 03:58:49', NULL),
(120, 24, 201, 212, 'Perfumes & Colognes', 'perfumescolognes', 'uploads/category/perfumes.jpg', '', 18, 1, 1, 0, 2, 1, '2018-05-10 15:28:16', '2019-04-06 06:08:35', NULL),
(121, 24, 157, 170, 'Personal Grooming', 'PersonalGrooming', 'uploads/category/personal-grooming.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:30:22', '2019-06-25 03:58:49', NULL),
(122, 24, 171, 182, 'Pharmaceuticals', 'Pharmaceuticals', 'uploads/category/pharmaceuticals.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:33:17', '2019-06-25 03:58:49', NULL),
(123, 22, 341, 358, 'Women\'s Clothing', 'Women-Clothing', 'uploads/category/women27s-clothing.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:35:31', '2019-10-03 07:04:13', NULL),
(124, 22, 409, 424, 'Women\'s Footwear', 'Women-Footwear', 'uploads/category/women27s-footwear.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:37:04', '2019-10-03 07:04:13', NULL),
(125, 22, 397, 408, 'Women\'s Accessories', 'Women-Accessories', 'uploads/category/women27s-accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:38:11', '2019-10-03 07:04:13', NULL),
(126, 22, 359, 372, 'Jewelery', 'Jewelery', 'uploads/category/jewelery.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:38:58', '2019-10-03 07:04:13', NULL),
(127, 22, 425, 444, 'Men\'s Clothing', 'MensClothing', 'uploads/category/men27s-clothing.jpg', '', 12, 1, 1, 0, 2, 1, '2018-05-10 15:41:33', '2019-10-03 07:04:13', NULL),
(128, 22, 387, 396, 'Men\'s Footwear', 'Men-Footwear', 'uploads/category/men27s-footwear.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:42:49', '2019-10-03 07:04:13', NULL),
(129, 22, 373, 386, 'Men\'s Accessories', 'Men-Accessories', 'uploads/category/men27s-accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 15:43:48', '2019-10-03 07:04:13', NULL),
(130, 26, 585, 594, 'Nutritions', 'Nutritions', 'uploads/category/nutritions-1.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:06:41', '2019-10-03 07:04:13', NULL),
(131, 26, 565, 584, 'Sports Equipment', 'SportsEquipment', 'uploads/category/sports-equipment.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:07:37', '2019-10-03 07:04:13', NULL),
(132, 26, 547, 552, 'Fitness Equipment', 'FitnessEquipment', 'uploads/category/fitness-equipment.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:08:18', '2019-10-03 07:04:13', NULL),
(133, 26, 553, 564, 'Sports Clothing', 'SportsClothing', 'uploads/category/sports-clothing.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:09:20', '2019-10-03 07:04:13', NULL),
(134, 27, 227, 228, 'Groceries', 'Groceries', 'uploads/category/download-3.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:11:01', '2019-10-03 07:04:13', NULL),
(135, 27, 217, 218, 'Hair Care', 'Hair-Care', 'uploads/category/haircare.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:13:38', '2019-06-25 04:13:09', '2019-06-25 04:13:09'),
(136, 27, 229, 230, 'Snacks, Sweets & Chocolates', 'Snacks-Sweets-Chocolates', 'uploads/category/fa32021c3f3c13e39ba55dc3aa0c26ea.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:17:12', '2019-10-03 07:04:13', NULL),
(137, 27, 243, 244, 'Body Care', 'BodyCare', 'uploads/category/body-care-gift-set-large.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:18:05', '2019-10-03 07:04:13', NULL),
(138, 27, 231, 232, 'Dental Care', 'Dental-Care', 'uploads/category/16b1c52c7daa49f8ae4f7f59a05155a8.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:19:46', '2019-10-03 07:04:13', NULL),
(139, 27, 233, 234, 'Frozen Goods', 'Frozen-Goods', 'uploads/category/download-4.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:20:16', '2019-10-03 07:04:13', NULL),
(140, 27, 235, 236, 'Bath & Shower', 'Bath-Shower', 'uploads/category/triolv470.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:21:34', '2019-10-03 07:04:13', NULL),
(141, 27, 237, 238, 'Household', 'Household', 'uploads/category/download-5.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:22:23', '2019-10-03 07:04:13', NULL),
(142, 27, 239, 240, 'Beverages', 'Beverages', 'uploads/category/images-5.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:23:04', '2019-10-03 07:04:13', NULL),
(143, 27, 241, 242, 'Pet Supplies', 'Pet-Supplies', 'uploads/category/pet420x42051.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-10 16:24:28', '2019-10-03 07:04:13', NULL),
(144, 1, 446, 545, 'Other', 'Other', 'uploads/category/other.jpg', '', 3, 1, 1, 0, 1, 1, '2018-05-15 17:27:57', '2019-10-03 07:04:13', NULL),
(147, 20, 605, 616, 'Gaming', 'gaming', 'uploads/category/gaming.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-17 12:50:59', '2019-10-03 07:04:13', NULL),
(148, 29, 258, 259, 'Projector', 'projecctor', 'uploads/category/ggg.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-17 12:51:59', '2018-05-17 12:52:49', '2018-05-17 12:52:49'),
(149, 110, 670, 671, 'Ovens', 'ovens', 'uploads/category/b58b56a2-5662-4a99-b4a8-11681a45a251114ac333cd98e3c3a7653d6f51d5efd84.jpeg', '', 3, 1, 1, 0, 3, 1, '2018-05-17 17:23:41', '2019-10-03 07:04:13', NULL),
(150, 36, 764, 765, 'Side by Side', 'Sidebyside', 'uploads/category/ddddd.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-21 10:33:35', '2019-10-03 07:04:13', NULL),
(151, 29, 628, 629, 'Projectors', 'projector', 'uploads/category/ggg.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 10:41:33', '2019-10-03 07:04:13', NULL),
(152, 50, 612, 613, 'Dryer', 'Dryer', 'uploads/category/883049414768lg.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:09:42', '2019-01-21 10:25:18', '2019-01-21 10:25:18'),
(153, 50, 602, 603, 'Washer Dryer', 'Washer-Dryers', 'uploads/category/ddddxx.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:15:42', '2019-10-03 07:04:13', NULL),
(154, 147, 606, 607, 'Mouse', 'Mouse', 'uploads/category/download.png', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:17:39', '2019-10-03 07:04:13', NULL),
(155, 147, 608, 609, 'Keyboards', 'Keyboards', 'uploads/category/ddsds.png', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:18:21', '2019-10-03 07:04:13', NULL),
(156, 147, 614, 615, 'Gaming Accessories', 'gamingaccessories', 'uploads/category/s-l300.jpg', '', 18, 1, 1, 1, 3, 1, '2018-05-21 12:19:33', '2019-10-03 07:04:13', NULL),
(157, 147, 612, 613, 'Gaming Headphones', 'GamingHeadphoness', 'uploads/category/download-1.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:23:35', '2019-10-03 07:04:13', NULL),
(158, 147, 610, 611, 'Monitors', 'Monitors', 'uploads/category/asus-rog-pg27vq-gaming-monitor-1000px-0006-v1.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:26:45', '2019-10-03 07:04:13', NULL),
(159, 20, 709, 718, 'Office Accessories', 'OfficeAccessories', 'uploads/category/office-accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-21 12:31:13', '2019-10-03 07:04:13', NULL),
(160, 159, 710, 711, 'Shredders', 'Shredders', 'uploads/category/s0432252sc7.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:32:49', '2019-10-03 07:04:13', NULL),
(161, 159, 712, 713, 'Servers', 'Servers', 'uploads/category/dsnew-printers-drawer-3-1c.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:39:02', '2019-10-03 07:04:13', NULL),
(162, 159, 714, 715, 'UPS', 'UPS', 'uploads/category/dddd.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:39:45', '2019-10-03 07:04:13', NULL),
(163, 102, 648, 649, 'Fans', 'Fansss', 'uploads/category/download-2.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:43:56', '2019-10-03 07:04:13', NULL),
(164, 102, 662, 663, 'Water Heaters', 'waterheaters', 'uploads/category/download-3.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-21 12:45:42', '2019-10-03 07:04:13', NULL),
(165, 102, 650, 651, 'Extension Cords', 'Extension-Cords', 'uploads/category/download-4.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:46:50', '2019-10-03 07:04:13', NULL),
(166, 102, 652, 653, 'Generators', 'Generators', 'uploads/category/download-5.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 12:48:36', '2019-10-03 07:04:13', NULL),
(167, 102, 660, 661, 'Vacuum Cleaners & Blowers', 'Vacuum-Cleanerssblowers', 'uploads/category/main-qimg-e23b5072062c308de96c6c8f1428e229.png', '', 18, 1, 1, 0, 3, 1, '2018-05-21 12:49:56', '2019-10-03 07:04:13', NULL),
(168, 110, 682, 683, 'Food Processors & Juicers', 'Food-Processorsjuicers', 'uploads/category/download-6.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-21 12:58:42', '2019-10-03 07:04:13', NULL),
(169, 110, 678, 679, 'Toasters, Waffle & Roti Makers', 'ToastersWafflerotiMakers', 'uploads/category/cuisinart-4-slice-belgium-waffle-maker1750px.jpg', '', 12, 1, 1, 0, 3, 1, '2018-05-21 13:00:38', '2019-10-03 07:04:13', NULL),
(170, 49, 820, 821, 'Corded & Cordless Phones', 'cordedcordless', 'uploads/category/dfff.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-21 13:03:04', '2019-10-03 07:04:13', NULL),
(171, 91, 808, 809, 'eBook Readers', 'eBook-Readers', 'uploads/category/red-sony-prs-t2.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 13:06:36', '2019-10-03 07:04:13', NULL),
(172, 91, 810, 811, 'iPad', 'ipad', 'uploads/category/kindle-paperwhite.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-21 13:08:19', '2019-10-03 07:04:13', NULL),
(173, 21, 785, 790, 'Land & Cordless Phones', 'Land-Cordless-Phones', 'uploads/category/land--cordless-phones.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-21 13:10:06', '2018-09-15 06:31:25', '2018-09-15 06:31:25'),
(174, 173, 786, 787, 'Corded Phones', 'Corded-Phones', 'uploads/category/download.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 13:11:08', '2018-09-15 06:31:24', '2018-09-15 06:31:24'),
(175, 173, 788, 789, 'Cordless Phones', 'Cordless-Phones', 'uploads/category/images.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 13:11:48', '2018-09-15 06:31:24', '2018-09-15 06:31:24'),
(176, 21, 793, 800, 'Smart Devices', 'Smart-Devices', 'uploads/category/smart-devices.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-21 13:30:02', '2019-10-03 07:04:13', NULL),
(177, 176, 794, 795, 'Smart Glasses', 'Smart-Glasses', 'uploads/category/android-60-m300-smartglasses-1.png', '', 3, 1, 1, 0, 3, 1, '2018-05-21 13:33:17', '2019-10-03 07:04:13', NULL),
(178, 176, 796, 797, 'Smart Bands', 'Smart-Bands', 'uploads/category/download-7.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 13:34:39', '2019-10-03 07:04:13', NULL),
(179, 176, 798, 799, 'Smart Watches', 'Smart-Watches', 'uploads/category/alu-rosegold-5901f6365f9b5810dc4a3b2e.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 13:36:01', '2019-10-03 07:04:13', NULL),
(180, 123, 342, 343, 'Sarees', 'Sarees', 'uploads/category/imageoriginal.jpeg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 14:46:21', '2019-10-03 07:04:13', NULL),
(181, 123, 344, 345, 'Shalwars', 'Shalwars', 'uploads/category/ic-07a-400x600.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 14:47:14', '2019-10-03 07:04:13', NULL),
(182, 123, 346, 347, 'Sportswear', 'Sportswearr', 'uploads/category/bia-brazil-le4067-pant.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 14:48:14', '2019-10-03 07:04:13', NULL),
(183, 123, 346, 347, 'Tops & T\'s', 'Tops', 'uploads/category/l-1169hey-booto-kill-a-mockingbirdmemory-boxwomens-book-t-shirt02large.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 14:49:41', '2019-09-16 12:17:32', '2019-09-16 12:17:32'),
(184, 123, 348, 349, 'Dresses', 'Dresses', 'uploads/category/31g9efkdklacul260sr200260.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 14:51:45', '2019-10-03 07:04:13', NULL),
(185, 123, 350, 351, 'Lingerie & Sleepwear', 'Lingerie-Sleepwear', 'uploads/category/adjustable-spaghetti-strap-black-satiny-tank-short-set.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 14:53:30', '2019-10-03 07:04:13', NULL),
(186, 123, 352, 353, 'Formal', 'Formal', 'uploads/category/2017-women-autumn-dress-suit-elegant-business-suits-blazer-formal-office-suits-work-tunics-pencil-dressjpg640x640.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 14:57:36', '2019-10-03 07:04:13', NULL),
(187, 123, 354, 355, 'Shorts & Skirts', 'Shorts-Skirts', 'uploads/category/1504831-1910-1-kalahari-jumpsuit-women-midnight-blue.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 15:00:31', '2019-10-03 07:04:13', NULL),
(188, 123, 356, 357, 'Jeans & Pants', 'Jeans-Pants', 'uploads/category/download-8.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 15:14:22', '2019-10-03 07:04:13', NULL),
(189, 36, 758, 759, 'Inverter', 'Inverter', 'uploads/category/download-9.jpg', '', 3, 1, 2, 0, 3, 1, '2018-05-21 15:16:49', '2019-10-03 07:04:13', NULL),
(190, 102, 654, 655, 'Polishers', 'Polishers', 'uploads/category/jijij.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 15:19:33', '2019-10-03 07:04:13', NULL),
(191, 102, 656, 657, 'High Pressure Washers', 'High-Pressure-Washers', 'uploads/category/61g5xfne5glsy355.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 15:48:09', '2019-10-03 07:04:13', NULL),
(192, 102, 658, 659, 'Sewing Machines', 'Sewing-Machines', 'uploads/category/download-10.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 15:54:22', '2019-10-03 07:04:13', NULL),
(193, 110, 686, 687, 'Fryers/Grills & Food Cookers', 'Fryer-foodcookers', 'uploads/category/0014374black-decker-health-grill-waffle-maker-black-lgm70-b5400.jpeg', '', 18, 1, 1, 0, 3, 1, '2018-05-21 16:02:54', '2019-10-03 07:04:13', NULL),
(194, 110, 672, 673, 'Coffee Makers', 'Coffee-Makers', 'uploads/category/the-11-best-coffee-makers-and-machines-2018.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 16:04:06', '2019-10-03 07:04:13', NULL),
(195, 95, 780, 781, 'Batteries', 'Batteries', 'uploads/category/panasonic-cordless-phone-compatible-nimh-battery-da-batt-1041024x1024.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 16:07:51', '2019-10-03 07:04:13', NULL),
(196, 95, 784, 785, 'Covers & Cases', 'Covers-Cases', 'uploads/category/leather.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 16:12:26', '2019-10-03 07:04:13', NULL),
(197, 95, 790, 791, 'Tempered Glass & Protectors', 'temperedglassprotectors', 'uploads/category/11646381.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-21 16:14:20', '2019-10-03 07:04:13', NULL),
(198, 95, 786, 787, 'OTG Pendrives & Flash drives', 'OTGpendrivesflashdrives', 'uploads/category/main-qimg-5d8c3cbbd640879f22676eb2333a6862-c.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 16:16:52', '2019-10-03 07:04:13', NULL),
(199, 124, 410, 411, 'Sandals', 'Sandals', 'uploads/category/il340x270646707906qb1l.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-21 17:54:59', '2019-10-03 07:04:13', NULL),
(200, 124, 412, 413, 'Wedges', 'Wedges', 'uploads/category/espadrilles-wedgeswomen-navy-palamos-viscata-0grande.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:05:54', '2019-10-03 07:04:13', NULL),
(201, 124, 414, 415, 'Flats & Ballets', 'Flats-Ballets', 'uploads/category/dfsfs.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:09:20', '2019-10-03 07:04:13', NULL),
(202, 1, 48, 49, 'Slippers & Flip Flops', 'Slipper-Flip-Flops', 'uploads/category/bbbbcvcv.jpg', '', 3, 2, 1, 0, 1, 1, '2018-05-22 12:13:03', '2018-07-31 09:25:27', '2018-05-24 13:32:34'),
(203, 124, 416, 417, 'Sports', 'Sports', 'uploads/category/riya-51-cgryrani-39-asian-grey-original-imaewney2shdcz2q.jpeg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:14:26', '2019-10-03 07:04:13', NULL),
(204, 124, 418, 419, 'Casual', 'Casual', 'uploads/category/ladies-casual-shoes-250x250.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:15:43', '2019-10-03 07:04:13', NULL),
(205, 124, 420, 421, 'Heels', 'Heels', 'uploads/category/blackheels.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:18:59', '2019-10-03 07:04:13', NULL),
(206, 125, 398, 399, 'Bags & Wallets', 'Bags-Wallets', 'uploads/category/jghgh.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:34:17', '2019-10-03 07:04:13', NULL),
(207, 125, 400, 401, 'Sunglasses', 'Sunglasses', 'uploads/category/41ect2n7mllsx342ql70.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:37:10', '2019-10-03 07:04:13', NULL),
(208, 125, 402, 403, 'Watches', 'Watches', 'uploads/category/35.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:38:15', '2019-10-03 07:04:13', NULL),
(209, 125, 404, 405, 'Belts', 'Belts', 'uploads/category/polo-belts-forwomen-pink-cream-purple-21.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:43:58', '2019-10-03 07:04:13', NULL),
(210, 125, 406, 407, 'Hair Accessories', 'Hair-Accessories', 'uploads/category/dddeerr.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 12:55:15', '2019-10-03 07:04:13', NULL),
(211, 126, 360, 361, 'Earrings', 'Earrings', 'uploads/category/aquamarine-drop-earrings-p28888-5701image.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 14:04:51', '2019-10-03 07:04:13', NULL),
(212, 126, 362, 363, 'Necklace', 'Necklace', 'uploads/category/product-hugerect-573807-209805-1444624428-27590751148be98684851fa0d7bcb8f0.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 14:45:27', '2019-10-03 07:04:13', NULL),
(213, 126, 364, 365, 'Bracelet', 'Bracelet', 'uploads/category/womens-id-bracelet-in-18k-gold-platingjumbo.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 14:46:43', '2019-10-03 07:04:13', NULL),
(214, 126, 366, 367, 'Rings', 'Rings', 'uploads/category/613z7eorv1lsy355.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-22 14:49:24', '2019-10-03 07:04:13', NULL),
(215, 126, 368, 369, 'Jewellery Set', 'Jewellery-Set', 'uploads/category/717epqppeqluy395.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 11:45:27', '2019-10-03 07:04:13', NULL),
(216, 126, 370, 371, 'Pendants', 'Pendants', 'uploads/category/jp03989-ygp9eb1lar.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 14:47:57', '2019-10-03 07:04:13', NULL),
(217, 127, 426, 427, 'T-Shirts', 'T-Shirts', 'uploads/category/61j1u9rfdplux385.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 14:49:11', '2019-10-03 07:04:13', NULL),
(218, 127, 428, 429, 'Shirts', 'Shirts', 'uploads/category/product-image-308811918large.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:01:01', '2019-10-03 07:04:13', NULL),
(219, 127, 430, 431, 'Trousers', 'Trousers', 'uploads/category/tp10050grey.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:02:41', '2019-10-03 07:04:13', NULL),
(220, 127, 432, 433, 'Jeans', 'Jeans', 'uploads/category/644279713pl.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:04:41', '2019-10-03 07:04:13', NULL),
(221, 127, 434, 435, 'Shorts', 'Shorts', 'uploads/category/47801718l.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:07:07', '2019-10-03 07:04:13', NULL),
(222, 127, 436, 437, 'Sportswear', 'Sportswear', 'uploads/category/mens-sportsuit-casual-tracksuit-plus-size-sportswear-men-tracksuit480x480.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:09:13', '2019-10-03 07:04:13', NULL),
(223, 127, 438, 439, 'Formal', 'Formall', 'uploads/category/liii.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:18:05', '2019-10-03 07:04:13', NULL),
(224, 127, 442, 443, 'Socks & Boxers', 'socksunderwears', 'uploads/category/mens-underwear-mens-boxers-mens-underwears.jpg', '', 18, 1, 1, 2, 3, 1, '2018-05-23 15:22:25', '2019-10-03 07:04:13', NULL),
(225, 127, 440, 441, 'Sweaters & Sweatshirts', 'Sweaters-Sweatshirts', 'uploads/category/2017-new-autumn-winter-round-neck-pullover-men-slim-fit-knitted-sweater-pull-homme-jersey-hombre.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:24:17', '2019-10-03 07:04:13', NULL),
(226, 128, 388, 389, 'Casual', 'Casuall', 'uploads/category/sku231756-7.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:36:03', '2019-10-03 07:04:13', NULL),
(227, 128, 390, 391, 'Formal', 'Formalll', 'uploads/category/formal-shoes-500x500.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:36:39', '2019-10-03 07:04:13', NULL),
(228, 128, 392, 393, 'Sports', 'Sportss', 'uploads/category/gggdf.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:38:16', '2019-10-03 07:04:13', NULL),
(229, 128, 394, 395, 'Slippers & Flip Flops', 'Slippers-Flip-Flopss', 'uploads/category/5yru.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:42:04', '2019-10-03 07:04:13', NULL),
(230, 129, 374, 375, 'Bags, Wallets & Backpacks', 'Bags-Wallets-Backpacks', 'uploads/category/efb27ea1c48eae9cf2e1a0793fade67d--cool-backpacks-men-bags.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:42:33', '2019-10-03 07:04:13', NULL),
(231, 129, 376, 377, 'Belts, Ties & Cufflinks', 'Belts, Ties & Cufflinks', 'uploads/category/blackleatherbelttierackaustralia-315x300.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:48:59', '2019-10-03 07:04:13', NULL),
(232, 129, 378, 379, 'Caps & Hats', 'Caps & Hats', 'uploads/category/gegeg.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:49:56', '2019-10-03 07:04:13', NULL),
(233, 129, 380, 381, 'Sunglasses', 'Sunglassess', 'uploads/category/00063914.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:52:55', '2019-10-03 07:04:13', NULL),
(234, 129, 382, 383, 'Watches', 'Watchess', 'uploads/category/173810841640.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 15:53:48', '2019-10-03 07:04:13', NULL),
(235, 103, 68, 69, 'Plates', 'Plates', 'uploads/category/yu.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 16:51:16', '2018-06-01 10:56:11', NULL),
(236, 103, 70, 71, 'Cups', 'Cups', 'uploads/category/urban-nature-culture-good-morning-cup-gift-edition.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 16:52:41', '2018-06-01 10:56:11', NULL),
(237, 103, 78, 79, 'Table Mats', 'TableMats', 'uploads/category/img32c.jpg', '', 12, 1, 1, 0, 3, 1, '2018-05-23 16:54:45', '2018-06-28 11:29:18', NULL),
(238, 103, 72, 73, 'Spoons', 'Spoons', 'uploads/category/5004c.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 16:56:36', '2018-06-28 11:29:18', NULL),
(239, 103, 74, 75, 'Forks', 'Forks', 'uploads/category/51farxoqt1lsl1500.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 16:58:44', '2018-06-28 11:29:18', NULL),
(240, 103, 76, 77, 'Knives', 'Knives', 'uploads/category/fghjhg.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 16:59:41', '2018-06-28 11:29:18', NULL),
(241, 104, 142, 143, 'Pots & Pans', 'potspans', 'uploads/category/dsdsdsd.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-23 17:01:26', '2019-01-22 04:31:14', NULL),
(242, 104, 138, 139, 'Knives, Scrapers, Choppers & Slicers', 'Knivessslicers', 'uploads/category/dddddsdss.jpg', '', 18, 1, 1, 0, 3, 1, '2018-05-23 17:02:31', '2019-01-22 04:31:14', NULL),
(243, 104, 128, 129, 'Cutting Boards', 'Cutting Boards', 'uploads/category/81pkz8bglsl1500.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 17:03:37', '2019-01-22 04:31:14', NULL),
(244, 104, 130, 131, 'Measuring Tools', 'Measuring Tools', 'uploads/category/asd.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 17:04:38', '2019-01-22 04:31:14', NULL),
(245, 104, 132, 133, 'Spoons', 'Spoonss', 'uploads/category/scscsc.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 17:05:32', '2019-01-22 04:31:14', NULL),
(246, 104, 134, 135, 'Baking Trays', 'Baking Trays', 'uploads/category/scsdsdds.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-23 17:06:32', '2019-01-22 04:31:14', NULL),
(247, 105, 48, 49, 'Mattresses', 'Mattresses', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:43:30', '2018-06-01 10:56:11', NULL),
(248, 105, 50, 51, 'Pillows', 'Pillows', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:44:38', '2018-06-01 10:56:11', NULL),
(249, 105, 52, 53, 'Sheets', 'Sheets', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:45:16', '2018-06-01 10:56:11', NULL),
(250, 105, 54, 55, 'Cushions', 'Cushions', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:45:40', '2018-06-01 10:56:11', NULL),
(251, 105, 56, 57, 'Shower Heads', 'Shower Heads', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:46:37', '2018-06-01 10:56:11', NULL),
(252, 105, 58, 59, 'Hand Showers', 'Hand Showers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:47:05', '2018-06-01 10:56:11', NULL),
(253, 105, 60, 61, 'Taps', 'Taps', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:47:51', '2018-06-01 10:56:11', NULL),
(254, 105, 62, 63, 'Shelves', 'Shelves', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:48:28', '2018-06-01 10:56:11', NULL),
(255, 105, 64, 65, 'Mirrors', 'Mirrors', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:48:58', '2018-06-01 10:56:11', NULL),
(256, 106, 82, 83, 'Wall Paints', 'Wall Paints', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:49:33', '2018-08-07 10:57:25', NULL),
(257, 106, 84, 85, 'Floor Paints', 'Floor Paints', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:49:57', '2018-08-07 10:57:25', NULL),
(258, 106, 86, 87, 'Fillers', 'Fillers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:50:18', '2018-08-07 10:57:25', NULL),
(259, 106, 88, 89, 'Brushes', 'Brushes', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:50:49', '2018-08-07 10:57:25', NULL),
(260, 106, 90, 91, 'Paint Holders', 'Paint Holders', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:51:38', '2018-08-07 10:57:25', NULL),
(261, 107, 110, 111, 'Sofas', 'Sofas', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:52:00', '2019-01-22 04:31:14', NULL),
(262, 107, 112, 113, 'Chairs', 'Chairs', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:52:21', '2019-01-22 04:31:14', NULL),
(263, 107, 114, 115, 'Tables', 'Tables', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:54:07', '2019-01-22 04:31:14', NULL),
(264, 107, 116, 117, 'Dining Tables', 'Dining Tables', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:54:33', '2019-01-22 04:31:14', NULL),
(265, 107, 118, 119, 'Beds', 'Beds', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:54:55', '2019-01-22 04:31:14', NULL),
(266, 107, 120, 121, 'Dressing Table', 'Dressing Table', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:55:24', '2019-01-22 04:31:14', NULL),
(267, 107, 122, 123, 'Mirrors', 'Mirrorss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:58:43', '2019-01-22 04:31:14', NULL),
(268, 108, 102, 103, 'Bulbs', 'Bulbs', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 11:59:36', '2018-08-07 10:57:25', NULL),
(269, 108, 104, 105, 'Chandeliers', 'Chandeliers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:00:31', '2018-08-07 10:57:25', NULL),
(270, 108, 106, 107, 'Lights & Lamps', 'lightslamps', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 12:00:52', '2019-01-22 04:31:14', NULL),
(271, 108, 106, 107, 'Decorations', 'Decorations', '', '', 3, 1, 2, 0, 3, 1, '2018-05-24 12:01:59', '2019-01-22 04:31:14', '2019-01-22 04:31:14'),
(272, 109, 94, 95, 'Drills', 'Drills', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:02:28', '2018-08-07 10:57:25', NULL),
(273, 109, 98, 99, 'Tools', 'toolssss', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 12:02:57', '2018-11-13 05:46:45', NULL),
(274, 109, 96, 97, 'Ladders', 'Ladders', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:04:07', '2018-11-13 05:46:45', NULL),
(275, 119, 150, 151, 'Lipstick', 'Lipstick', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:05:08', '2018-09-10 08:59:08', '2018-09-10 08:59:08'),
(276, 119, 152, 153, 'Lip', 'Lip', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 12:05:57', '2019-01-22 04:31:14', NULL),
(277, 119, 148, 149, 'Nail Polish', 'Nail-Polish', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:06:25', '2019-01-22 04:31:14', NULL),
(278, 119, 150, 151, 'Eye', 'Eye', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 12:07:03', '2019-01-22 04:31:14', NULL),
(279, 120, 202, 203, 'Women\'s', 'Womens', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:07:32', '2019-04-06 06:07:34', NULL),
(280, 120, 204, 205, 'Men\'s', 'Mens', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:08:01', '2019-04-06 06:07:34', NULL),
(281, 120, 206, 207, 'Kid\'s', 'Kid', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:08:29', '2019-04-06 06:07:34', NULL),
(282, 120, 208, 209, 'Branded', 'Branded', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:10:19', '2019-04-06 06:07:34', NULL),
(283, 120, 210, 211, 'Local', 'Local', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:10:47', '2019-04-06 06:07:34', NULL),
(284, 121, 158, 159, 'Hair', 'Hair', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:11:39', '2019-06-25 03:58:49', NULL),
(285, 121, 160, 161, 'Body', 'Body', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:12:03', '2019-06-25 03:58:49', NULL),
(286, 121, 164, 165, 'Sexual Wealth', 'Sexualwealth', '', '', 3, 1, 2, 0, 3, 1, '2018-05-24 12:12:25', '2019-09-16 07:53:27', NULL),
(287, 121, 162, 163, 'Foot', 'Foot', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:12:49', '2019-06-25 03:58:49', NULL),
(288, 121, 166, 167, 'Shavers & Trimmers', 'Shaverstrimmers', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 12:13:11', '2019-06-25 03:58:49', NULL),
(289, 121, 168, 169, 'Grooming Kits & Organizers', 'Groomingkitsorganizers', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 12:13:39', '2019-06-25 03:58:49', NULL),
(290, 122, 178, 179, 'Vitamins & Supplements', 'vitamins&Supplements', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 12:15:04', '2019-06-25 03:58:49', NULL),
(291, 122, 172, 173, 'Medication', 'Medication', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:15:30', '2019-06-25 03:58:49', NULL),
(292, 122, 180, 181, 'Repellents & Killers', 'Repellentskillers', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 12:15:54', '2019-06-25 03:58:49', NULL),
(293, 122, 174, 175, 'Wellness', 'Wellness', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:16:18', '2019-06-25 03:58:49', NULL),
(294, 122, 176, 177, 'Devices & Massagers', 'Devicesnmassagers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:16:52', '2019-06-25 03:58:49', NULL),
(295, 112, 248, 249, 'Diapers', 'Diapers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:19:21', '2019-10-03 07:04:13', NULL),
(296, 112, 256, 257, 'Strollers & Baby Seats', 'Strollersbabyseat', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 12:19:43', '2019-10-03 07:04:13', NULL),
(297, 112, 258, 259, 'Baby Beds & Playpen', 'Babybeds', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 12:20:10', '2019-10-03 07:04:13', NULL),
(298, 112, 250, 251, 'Wash Basins', 'Wash Basins', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:20:31', '2019-10-03 07:04:13', NULL),
(299, 112, 252, 253, 'Wipes', 'Wipes', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:20:53', '2019-10-03 07:04:13', NULL),
(300, 112, 254, 255, 'Carriers', 'Carriers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:21:15', '2019-10-03 07:04:13', NULL),
(301, 113, 288, 289, 'Sterilizers', 'Sterilizers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:21:43', '2019-10-03 07:04:13', NULL),
(302, 113, 290, 291, 'Bottles', 'Bottles', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:22:16', '2019-10-03 07:04:13', NULL),
(303, 113, 292, 293, 'Sippers & Cups', 'Sippers & Cups', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:22:54', '2019-10-03 07:04:13', NULL),
(304, 114, 306, 307, 'Frocks', 'Frocks', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:23:41', '2019-10-03 07:04:13', NULL),
(305, 114, 308, 309, 'T-Shirts', 'T-Shirtss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:24:37', '2019-10-03 07:04:13', NULL),
(306, 114, 310, 311, 'Trousers', 'Trouserss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:25:21', '2019-10-03 07:04:13', NULL),
(307, 114, 312, 313, 'Shorts', 'Shortss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:26:29', '2019-10-03 07:04:13', NULL),
(308, 114, 314, 315, 'Caps & Hats', 'Caps & Hatss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:28:02', '2019-10-03 07:04:13', NULL),
(309, 115, 278, 279, 'Shampoo', 'Shampoo', '', '', 3, 1, 2, 0, 3, 1, '2018-05-24 12:29:49', '2019-10-03 07:04:13', NULL),
(310, 115, 280, 281, 'Soap & Shampoo', 'soapshampoo', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 12:30:34', '2019-10-03 07:04:13', NULL),
(311, 115, 282, 283, 'Lotions & Oil', 'Lotionoil', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 12:31:27', '2019-10-03 07:04:13', NULL),
(312, 25, 295, 304, 'Furniture', 'Furnituree', 'uploads/category/furniture.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-24 12:32:51', '2019-10-03 07:04:13', NULL),
(313, 312, 296, 297, 'Stools', 'Stools', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:33:38', '2019-10-03 07:04:13', NULL),
(314, 312, 300, 301, 'Tables & Chairs', 'Tablesschairs', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 12:34:10', '2019-10-03 07:04:13', NULL),
(315, 312, 302, 303, 'Cupboards', 'Cupboards', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 12:35:18', '2019-10-03 07:04:13', NULL),
(316, 312, 298, 299, 'Walkers', 'Walkers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:35:42', '2019-10-03 07:04:13', NULL),
(317, 117, 262, 263, 'T-Shirts', 'T-Shirtsss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:38:17', '2019-10-03 07:04:13', NULL),
(318, 117, 264, 265, 'Shorts', 'Shortssss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:38:41', '2019-10-03 07:04:13', NULL),
(319, 117, 266, 267, 'Bibs', 'Bibs', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:39:12', '2019-10-03 07:04:13', NULL),
(320, 117, 268, 269, 'Shoes', 'Shoess', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:39:46', '2019-10-03 07:04:13', NULL),
(321, 117, 270, 271, 'Slippers', 'Slippersss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:40:18', '2019-10-03 07:04:13', NULL),
(322, 117, 272, 273, 'Frocks', 'Frockss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:40:46', '2019-10-03 07:04:13', NULL),
(323, 117, 274, 275, 'Caps & Hats', 'Caps & Hatsss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:41:12', '2019-10-03 07:04:13', NULL),
(324, 118, 332, 333, 'Breast Pumps', 'Breast Pumps', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:41:43', '2019-10-03 07:04:13', NULL),
(325, 118, 336, 337, 'Nutritions', 'Nutritionsss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:42:09', '2019-10-03 07:04:13', NULL),
(326, 118, 334, 335, 'Maternity Clothing', 'Maternity Clothing', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:42:38', '2019-10-03 07:04:13', NULL),
(327, 116, 318, 319, 'Action Toys', 'Action-Toys', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:43:44', '2019-10-03 07:04:13', NULL),
(328, 116, 320, 321, 'Infant Toys', 'Infant-Toys', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:44:16', '2019-10-03 07:04:13', NULL),
(329, 116, 322, 323, 'Dolls & Soft Toys', 'Dolls-Soft-Toys', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:44:44', '2019-10-03 07:04:13', NULL),
(330, 116, 324, 325, 'Building & Educational Toys', 'Building-Educational-Toys', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:45:12', '2019-10-03 07:04:13', NULL),
(331, 116, 326, 327, 'Games & Puzzles', 'Games-Puzzles', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:45:43', '2019-10-03 07:04:13', NULL),
(332, 116, 328, 329, 'Wooden Toys', 'Wooden-Toys', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:46:11', '2019-10-03 07:04:13', NULL);
INSERT INTO `categories` (`id`, `parent_id`, `lft`, `rgt`, `name`, `slug`, `image`, `banner_image`, `user_id`, `is_display`, `status_id`, `is_display_banner`, `depth`, `order_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(333, 130, 592, 593, 'Supplements', 'Supplementsss', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 12:47:31', '2019-10-03 07:04:13', NULL),
(334, 130, 586, 587, 'Fat Burners', 'Fat-Burners', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:51:49', '2019-10-03 07:04:13', NULL),
(335, 130, 588, 589, 'Energy Drinks', 'Energy-Drinks', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:52:21', '2019-10-03 07:04:13', NULL),
(336, 130, 590, 591, 'Edible Energy', 'Edible-Energy', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:53:10', '2019-10-03 07:04:13', NULL),
(337, 131, 566, 567, 'Badminton', 'Badminton', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:53:40', '2019-10-03 07:04:13', NULL),
(338, 131, 568, 569, 'Cricket', 'Cricket', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:54:34', '2019-10-03 07:04:13', NULL),
(339, 131, 570, 571, 'Basketball', 'Basketball', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:55:42', '2019-10-03 07:04:13', NULL),
(340, 131, 572, 573, 'Football', 'Football', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:56:26', '2019-10-03 07:04:13', NULL),
(341, 131, 574, 575, 'Rugby', 'Rugby', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:57:34', '2019-10-03 07:04:13', NULL),
(342, 131, 576, 577, 'Volleyball', 'Volleyball', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 12:58:36', '2019-10-03 07:04:13', NULL),
(343, 131, 578, 579, 'Swimming', 'Swimming', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:01:28', '2019-10-03 07:04:13', NULL),
(344, 131, 580, 581, 'Tennis', 'Tennis', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:02:16', '2019-10-03 07:04:13', NULL),
(345, 131, 582, 583, 'Squash', 'Squash', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:03:15', '2019-10-03 07:04:13', NULL),
(346, 132, 548, 549, 'Orbitreks & Treadmills', 'orbitreksTreadmills', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:04:25', '2019-10-03 07:04:13', NULL),
(347, 132, 550, 551, 'Other', 'otherrrr', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:05:25', '2019-10-03 07:04:13', NULL),
(348, 133, 554, 555, 'Men\'s', 'Mensss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:06:46', '2019-10-03 07:04:13', NULL),
(349, 133, 556, 557, 'Women\'s', 'Womensss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:07:48', '2019-10-03 07:04:13', NULL),
(350, 133, 558, 559, 'T-Shirts', 'T-Shirtssss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:10:28', '2019-10-03 07:04:13', NULL),
(351, 133, 560, 561, 'Shorts', 'Shortsss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:11:27', '2019-10-03 07:04:13', NULL),
(352, 133, 562, 563, 'Underwear', 'Underwearr', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:12:12', '2019-10-03 07:04:13', NULL),
(353, 24, 183, 192, 'Bath & Skin Care', 'Bath&Skinss', 'uploads/category/bath--skin-care-1.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-24 13:15:26', '2019-06-25 03:58:49', NULL),
(354, 353, 184, 185, 'Shampoo & Conditioner', 'Shampoooo', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 13:16:41', '2019-06-25 03:58:49', NULL),
(355, 353, 186, 187, 'Moizturizers, Creams & Lotions', 'MoizturizersCreamsLotions', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 13:17:43', '2019-06-25 03:58:49', NULL),
(356, 115, 284, 285, 'Perfumes & Deodrants', 'perfumesdeodrants', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 13:18:32', '2019-10-03 07:04:13', NULL),
(357, 353, 188, 189, 'Soap & Face Wash', 'soapfacewash', '', '', 18, 1, 1, 2, 3, 1, '2018-05-24 13:19:57', '2019-06-25 03:58:49', NULL),
(358, 24, 193, 200, 'Body Care', 'BodyCaree', 'uploads/category/body-care.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-24 13:24:21', '2019-04-06 06:07:34', NULL),
(359, 358, 196, 197, 'Deodarant & Spray', 'Deodarantspray', '', '', 12, 1, 1, 0, 3, 1, '2018-05-24 13:26:19', '2019-04-06 06:07:34', NULL),
(360, 358, 194, 195, 'Body Wash', 'Body-Wash', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:27:40', '2019-04-06 06:07:34', NULL),
(361, 358, 198, 199, 'Lotions & Oil', 'Lotionsss', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 13:29:51', '2019-04-06 06:07:34', NULL),
(362, 144, 447, 452, 'China Town', 'Chinatown', 'uploads/category/china-town.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-24 13:30:56', '2019-10-03 07:04:13', NULL),
(363, 124, 422, 423, 'Slippers & Flip Flops', 'Slippers-Flip-Flopssss', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:33:52', '2019-10-03 07:04:13', NULL),
(364, 362, 450, 451, 'Replicas', 'replicas', '', '', 18, 1, 1, 0, 3, 1, '2018-05-24 13:37:17', '2019-10-03 07:04:13', NULL),
(365, 362, 448, 449, 'Fakes', 'Fakes', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:38:09', '2019-10-03 07:04:13', NULL),
(366, 144, 453, 460, 'Gadgets', 'Gadgets', 'uploads/category/gadgets.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-24 13:41:01', '2019-10-03 07:04:13', NULL),
(367, 366, 458, 459, 'Toys', 'Toyss', 'uploads/category/toys--games.jpg', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:42:03', '2019-10-03 07:04:13', NULL),
(368, 366, 454, 455, 'New Tech', 'new-tech', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:42:53', '2019-10-03 07:04:13', NULL),
(369, 366, 456, 457, 'Smart Home Devices', 'Smart-Home-Devices', '', '', 3, 1, 1, 0, 3, 1, '2018-05-24 13:45:02', '2019-10-03 07:04:13', NULL),
(370, 144, 461, 472, 'Musical Equipment & Accessories', 'MusicalEquipment&Accessories', 'uploads/category/musical-equipment--accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-05-24 13:46:21', '2019-10-03 07:04:13', NULL),
(371, 29, 632, 633, 'Bluetooth Speakers', 'BluetoothSpeakers', '', '', 3, 1, 1, 0, 3, 1, '2018-05-28 17:38:26', '2019-10-03 07:04:13', NULL),
(372, 144, 473, 482, 'Books', 'books', 'uploads/category/books.jpg', '', 3, 1, 1, 0, 2, 1, '2018-06-01 16:34:25', '2019-10-03 07:04:13', NULL),
(373, 144, 483, 494, 'Automotive', 'Automotive', 'uploads/category/automotive.jpg', '', 3, 1, 1, 0, 2, 1, '2018-06-01 16:35:16', '2019-10-03 07:04:13', NULL),
(374, 144, 535, 544, 'Stationary & Office Supplies', 'Staionary&OfficeSupplies', 'uploads/category/staionary--office-supplies.jpg', '', 18, 1, 1, 0, 2, 1, '2018-06-01 16:36:05', '2019-10-03 07:04:13', NULL),
(375, 144, 495, 506, 'Party Supplies', 'PartySupplies', 'uploads/category/party-supplies.jpg', '', 3, 1, 1, 0, 2, 1, '2018-06-01 16:37:19', '2019-10-03 07:04:13', NULL),
(376, 144, 507, 526, 'Gift Packs', 'GiftPacks', 'uploads/category/gift-packs.jpg', '', 3, 1, 1, 0, 2, 1, '2018-06-01 16:38:51', '2019-10-03 07:04:13', NULL),
(377, 144, 527, 532, 'Travel Accessories', 'TravelAccessories', 'uploads/category/travel-accessories.jpg', '', 3, 1, 1, 0, 2, 1, '2018-06-01 16:39:43', '2019-10-03 07:04:13', NULL),
(378, 144, 533, 534, 'Second Hand', 'SecondHand', 'uploads/category/second-hand.jpg', '', 3, 1, 1, 0, 2, 1, '2018-06-01 16:41:14', '2019-10-03 07:04:13', NULL),
(379, 370, 462, 463, 'Guitars', 'Guitars', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:10:24', '2019-10-03 07:04:13', NULL),
(380, 370, 464, 465, 'Organs', 'organs', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:11:49', '2019-10-03 07:04:13', NULL),
(381, 370, 466, 467, 'Violins', 'Violins', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:12:17', '2019-10-03 07:04:13', NULL),
(382, 370, 468, 469, 'Flutes', 'Flutes', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:12:42', '2019-10-03 07:04:13', NULL),
(383, 370, 470, 471, 'Music Accessories', 'MusicAccessories', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:13:33', '2019-10-03 07:04:13', NULL),
(384, 372, 474, 475, 'Novels', 'Novels', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:14:03', '2019-10-03 07:04:13', NULL),
(385, 372, 476, 477, 'Education', 'Education', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:14:29', '2019-10-03 07:04:13', NULL),
(386, 372, 478, 479, 'Magazines', 'Magazines', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:15:03', '2019-10-03 07:04:13', NULL),
(387, 373, 484, 485, 'Tires', 'Tires', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:18:15', '2019-10-03 07:04:13', NULL),
(388, 373, 486, 487, 'Spares', 'Spares', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:18:54', '2019-10-03 07:04:13', NULL),
(389, 373, 488, 489, 'Tools', 'Tools', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:19:30', '2019-10-03 07:04:13', NULL),
(390, 373, 490, 491, 'Liquids', 'Liquids', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:23:16', '2019-10-03 07:04:13', NULL),
(391, 374, 536, 537, 'Pens', 'Pens', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:27:37', '2019-10-03 07:04:13', NULL),
(392, 374, 538, 539, 'Pencils', 'Pencils', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:28:14', '2019-10-03 07:04:13', NULL),
(393, 374, 540, 541, 'Papers', 'Papers', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:32:21', '2019-10-03 07:04:13', NULL),
(394, 374, 542, 543, 'Tape', 'Tape', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:42:50', '2019-10-03 07:04:13', NULL),
(395, 372, 480, 481, 'Staplers & Pins', 'Staplers-Pins', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:43:24', '2019-10-03 07:04:13', NULL),
(396, 375, 496, 497, 'Baloons', 'Baloons', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:46:00', '2019-10-03 07:04:13', NULL),
(397, 375, 500, 501, 'Signs', 'Signs', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:46:29', '2019-10-03 07:04:13', NULL),
(398, 375, 498, 499, 'Candles', 'Candles', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:47:12', '2019-10-03 07:04:13', NULL),
(399, 375, 502, 503, 'Party Signs', 'PartySigns', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:51:51', '2019-10-03 07:04:13', NULL),
(400, 376, 522, 523, 'Bags', 'bagssss', '', '', 18, 1, 1, 0, 3, 1, '2018-06-01 17:53:20', '2019-10-03 07:04:13', NULL),
(401, 376, 508, 509, 'For Her', 'ForHer', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:54:10', '2019-10-03 07:04:13', NULL),
(402, 376, 510, 511, 'Baby', 'Baby', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:54:35', '2019-10-03 07:04:13', NULL),
(403, 376, 512, 513, 'Kids', 'Kids', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:54:59', '2019-10-03 07:04:13', NULL),
(404, 376, 514, 515, 'Family', 'Family', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:55:22', '2019-10-03 07:04:13', NULL),
(405, 376, 516, 517, 'Weddings', 'Weddings', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:55:49', '2019-10-03 07:04:13', NULL),
(406, 376, 518, 519, 'Birthdays', 'Birthdays', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:56:18', '2019-10-03 07:04:13', NULL),
(407, 376, 520, 521, 'Anniversary', 'Anniversary', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:56:50', '2019-10-03 07:04:13', NULL),
(408, 377, 528, 529, 'Bags', 'Bags', '', '', 3, 1, 1, 0, 3, 1, '2018-06-01 17:58:17', '2019-10-03 07:04:13', NULL),
(409, 377, 530, 531, 'Umbrellas', 'Umbrellas', '', '', 12, 1, 1, 0, 3, 1, '2018-06-01 17:58:45', '2019-10-03 07:04:13', NULL),
(410, 373, 492, 493, 'Accessories', 'Accessoriesss', '', '', 3, 1, 1, 0, 3, 1, '2018-06-02 10:45:27', '2019-10-03 07:04:13', NULL),
(411, 159, 716, 717, 'Accessories', 'Accessoriesssss', '', '', 3, 1, 1, 0, 3, 1, '2018-06-02 13:08:57', '2019-10-03 07:04:13', NULL),
(412, 23, 129, 130, 'Home Accessories', 'HomeAccessoriess', '', '', 3, 1, 2, 0, 2, 1, '2018-06-04 13:27:16', '2018-08-07 10:59:16', '2018-08-07 10:59:16'),
(413, 110, 674, 675, 'Hand Mixers', 'HandMixers', '', '', 3, 1, 1, 0, 3, 1, '2018-06-05 15:49:34', '2019-10-03 07:04:13', NULL),
(414, 104, 140, 141, 'Other', 'kitchenother', '', '', 18, 1, 1, 0, 3, 999, '2018-06-05 16:03:22', '2019-09-25 09:46:00', NULL),
(415, 104, 136, 137, 'Kettles & Flasks', 'Kettlesnflasks', '', '', 3, 1, 1, 0, 3, 1, '2018-06-05 16:31:32', '2019-01-22 04:31:14', NULL),
(416, 129, 384, 385, 'Other', 'otherrr', '', '', 1, 1, 2, 0, 3, 1, '2018-06-06 14:16:09', '2019-10-03 07:04:13', NULL),
(417, 107, 124, 125, 'Carpets', 'Carpets', '', '', 3, 1, 1, 0, 3, 1, '2018-06-08 11:48:47', '2019-01-22 04:31:14', NULL),
(418, 376, 524, 525, 'For Him', 'forhim', '', '', 18, 1, 1, 0, 3, 1, '2018-09-12 05:27:03', '2019-10-03 07:04:13', NULL),
(419, 119, 154, 155, 'Makeup Accessories', 'Aaccessoriess', '', '', 18, 1, 1, 0, 3, 1, '2018-09-14 04:40:24', '2019-01-22 04:31:14', NULL),
(420, 353, 190, 191, 'Face & Skin', 'facenskin', '', '', 18, 1, 1, 1, 3, 1, '2018-09-14 04:53:05', '2019-06-25 03:58:49', NULL),
(421, 75, 729, 730, 'Mackbooks', 'macbook', '', '', 18, 1, 1, 0, 4, 1, '2018-09-19 06:19:53', '2018-09-19 06:27:00', '2018-09-19 06:27:00'),
(422, 80, 704, 705, 'iMac\'s', 'iMacs', '', '', 18, 1, 1, 0, 3, 1, '2018-09-19 09:05:59', '2019-10-03 07:04:13', NULL),
(423, 110, 680, 681, 'Cooker Hoods & Hobs', 'cookerhoodhobs', '', '', 18, 1, 1, 0, 3, 1, '2018-11-12 04:57:42', '2019-10-03 07:04:13', NULL),
(424, 375, 504, 505, 'Christmas Trees & Deco\'s', 'xmas', '', '', 18, 1, 1, 0, 3, 1, '2018-11-26 07:04:19', '2019-10-03 07:04:13', NULL),
(425, 1, 816, 817, 'NDB Offer', 'saleoffer', 'uploads/category/sdsd.png', 'uploads/category-banner/rerer.png', 18, 2, 2, 2, 1, 1, '2019-05-22 10:36:50', '2019-08-02 10:43:50', '2019-08-02 10:43:50'),
(426, 1, 824, 825, 'Others', 'others', '', '', 33, 2, 1, 0, 1, 1, '2019-05-22 11:32:02', '2019-10-03 07:04:13', NULL),
(427, 1, 820, 821, 'test', 'test1', 'uploads/category/category-icon.png', 'uploads/category-banner/inverter-category-banner-1.jpg', 4, 1, 1, 2, 1, 1, '2019-05-30 04:08:25', '2019-05-30 04:12:01', '2019-05-30 04:12:01'),
(428, 1, 826, 827, 'Daily Mirror Promo', 'dmo', '', 'uploads/category-banner/1122.PNG', 18, 2, 2, 2, 1, 1, '2019-08-02 10:44:15', '2019-10-03 07:04:13', NULL),
(429, 1, 828, 829, 'Seylan Offers', 'seylanoffers', '', 'uploads/category-banner/seylan-15-discount-06.png', 18, 2, 2, 2, 1, 1, '2019-08-08 10:28:13', '2019-10-03 07:04:13', NULL),
(430, 1, 832, 833, 'NTB Amex 30% Offers', 'ntbamexbankoffer', 'uploads/category/union.png', 'uploads/category-banner/asfgsdbfrdb.png', 18, 1, 1, 1, 1, 1, '2019-08-29 04:37:47', '2019-10-30 18:35:13', NULL),
(431, 1, 830, 831, 'Buy 1 Pick 1', 'buy1pick1', 'uploads/category/download-1.png', '', 18, 2, 1, 2, 1, 1, '2019-08-31 07:47:58', '2019-10-03 07:04:13', NULL),
(432, 24, 213, 224, 'The Face Shop', 'thefaceshop', 'uploads/category/the-face-shop-logo-v1.png', 'uploads/category-banner/tfs--category-banner.jpg', 18, 1, 1, 1, 2, 1, '2019-09-05 05:17:58', '2019-10-03 07:04:13', NULL),
(433, 432, 214, 215, 'Mask', 'mask', 'uploads/category/8806182560040large.jpg', '', 18, 1, 1, 2, 3, 1, '2019-09-16 11:24:33', '2019-09-16 11:31:37', NULL),
(434, 432, 216, 217, 'Bodycare', 'tfsbodycare', '', '', 18, 1, 1, 2, 3, 1, '2019-09-16 11:26:49', '2019-09-16 11:31:36', NULL),
(435, 432, 218, 219, 'Cleansing', 'cleansing', '', '', 18, 1, 1, 2, 3, 1, '2019-09-16 11:27:07', '2019-09-16 11:31:35', NULL),
(436, 432, 220, 221, 'Skincare', 'tfsskincare', '', '', 18, 1, 1, 2, 3, 1, '2019-09-16 11:27:21', '2019-09-16 11:31:34', NULL),
(437, 432, 222, 223, 'Offers', 'thefaceshopoffer', 'uploads/category/the-face-shop-logo-v1.png', '', 18, 1, 1, 2, 3, 1, '2019-10-03 07:04:13', '2019-10-03 08:31:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `image`, `created_at`, `updated_at`) VALUES
(1, 'abans', 'abans@ymail.com', 'abance.jpg', '2019-08-30 00:15:33', '2019-08-30 00:45:11');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `company_id`, `fname`, `lname`, `personal_email`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 1, 'isharaa', 'sewwandi', 'ish@gmail.com', '0113244532', '2019-08-30 01:40:45', '2019-08-30 02:49:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_30_051423_create_companies_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ishara', 'admin@admin.com', NULL, '$2y$10$nHEwApYoWK4VZ2utN3lqxOUxtwLCGcTZEhIx1YNqGJmhyLGLO9U2e', NULL, '2019-08-29 23:34:46', '2019-08-29 23:34:46'),
(2, 'ishara', 'yaisharasewwandi@gmail.com', NULL, '$2y$10$04ukfSrIiGySKQ5BLwBZhuMyDDATBNFr/0h13NUyjZZh9ZAXdGMyC', NULL, '2020-01-14 12:27:25', '2020-01-14 12:27:25');

-- --------------------------------------------------------

--
-- Table structure for table `web_banner`
--

CREATE TABLE `web_banner` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image_path` text NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_banner`
--

INSERT INTO `web_banner` (`id`, `name`, `image_path`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 'uploads/company/abans.jpg', 1, '2020-01-15 06:24:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'banner', 'uploads/company/abans.jpg', 1, '2020-01-15 06:24:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `slug_url_UNIQUE` (`slug`) USING BTREE,
  ADD KEY `categories_parent_id_index` (`parent_id`) USING BTREE,
  ADD KEY `categories_lft_index` (`lft`) USING BTREE,
  ADD KEY `categories_rgt_index` (`rgt`) USING BTREE,
  ADD KEY `categories_user_id_index` (`user_id`) USING BTREE,
  ADD KEY `idx_catestatus` (`parent_id`,`deleted_at`,`status_id`,`is_display`) USING BTREE;

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `web_banner`
--
ALTER TABLE `web_banner`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=438;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_banner`
--
ALTER TABLE `web_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
