<?php

namespace App\Providers;

use App\Category;
use App\Webbanner;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Request;


class TestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.employee.index', function ($view) {
            $test = strtolower($this->app->request->getRequestUri());
            $tests = str_replace('/', '', $test);

            $categoryImage = Webbanner::whereHas('categories', function ($q) use ($tests) {
                return $q->where('slug', $tests);
            })->whereStatusId(1)->latest()->take(1)->get()->pluck('image_path');

            View::share('categoryImages', $categoryImage);
        });
    }
}
