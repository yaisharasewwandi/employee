<?php

namespace App;
use App\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Webbanner extends Model
{
    use SoftDeletes;

	protected $table = "web_banner";
    protected $fillable=['category_id','name','image_path','status_id'];

    public function categories()
    {
    	return $this->belongsToMany(Category::class, 'banner_category')->withTimestamps();
    }
    protected $dates = ['deleted_at'];

}
