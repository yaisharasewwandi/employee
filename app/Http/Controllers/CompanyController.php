<?php

namespace App\Http\Controllers;

use App\Company;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $query = Company::query();
        if($search){
            $query =$query->Where('name','like','%'.$search.'%')->orWhere('email','like','%'.$search.'%');
        }
        $company = $query->paginate(10);
        return view('admin.company.index',compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'name' => 'required',
            
            
        ]);

        if ($validator->fails()) {
            return redirect(route('company.create'))
                ->withErrors($validator)
                ->withInput();
        }

        $company=Company::create($request->all());
         $file = $request->file('image');
        if (!empty($file)) {
            $destinationPath = 'uploads/company/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $company->image = $filename;
            $company->save();
        }
        return redirect(route('company.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
          $validator = Validator::make($request->all(), [
            'name' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()
                ->withErrors($validator)
                ->withInput();
        }


        $company->name=$request->name;
        $company->email=$request->email;
        

        $file = $request->file('image');
        if (!empty($file)) {
            $destinationPath = 'uploads/company/';
            File::delete($destinationPath.$company->image);//delete current image from storage
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $company->image = $filename;
        }


        $company->save();
        return redirect(route('company.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
