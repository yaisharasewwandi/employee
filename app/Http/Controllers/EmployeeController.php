<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $company = Company::pluck('name', 'id');

         
        $search = $request->search;
        $query = Employee::query();
        if($search){
            $query =$query->Where('fname','like','%'.$search.'%')->orWhere('lname','like','%'.$search.'%');
        }
        $employee = $query->paginate(10);
        return view('admin.employee.index',compact('employee','company'));

        //  $employee = \DB::table('companies')
        // ->join('employees', 'company_id', '=', 'companies.id')
        // ->where('companies.id')
        // ->paginate(10);
       
        
        // return view('admin.employee.index',compact('employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $company = Company::pluck('name', 'id');

         $selectedID = 2;
            // return $posts;
        return view('admin.employee.create', compact('selectedID', 'company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = Employee::create($request->all());
        return redirect(route('employee.create'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
         $company = Company::pluck('name', 'id');

         $selectedID = 2;
        return view('admin.employee.edit',compact('employee','company','selectedID'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $validator = Validator::make($request->all(), [
            'fname' => 'required',
            'lname' => 'required',
            'company_id' => 'required',
            'personal_email' => 'required',
            'phone_number' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()
                ->withErrors($validator)
                ->withInput();
        }


        $employee->fname=$request->fname;
        $employee->lname=$request->lname;
        $employee->company_id=$request->company_id;
        $employee->personal_email=$request->personal_email;
        $employee->phone_number=$request->phone_number;
    


        $employee->save();
        return redirect(route('employee.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
         $employee = Employee::findOrFail($id);
    $employee->delete();

    return redirect(route('employee.index'));
    }
}
