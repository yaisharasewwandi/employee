<?php

namespace App;

use App\Webbanner;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable=['parent_id','lft','rgt','name','slug','image'];

    public function webbanner()
    {
    	return $this->belongsToMany(Webbanner::class, 'banner_category')->withTimestamps();
    }

}
